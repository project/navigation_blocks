/*!
 * Bootstrap Table of Contents v0.4.1 (http://afeld.github.io/bootstrap-toc/)
 * Copyright 2015 Aidan Feldman
 * Licensed under MIT (https://github.com/afeld/bootstrap-toc/blob/gh-pages/LICENSE.md) */

/**
 * Collection Bar logic.
 *
 */

(function ($) {
    'use strict';

    window.Toc = {
        helpers: {
            titleAttribute: 'data-toc-text',
            listClass: '',
            linkClass: '',
            // return all matching elements in the set, or their descendants
            findOrFilter: function ($el, selector) {
                // http://danielnouri.org/notes/2011/03/14/a-jquery-find-that-also-finds-the-root-element/
                // http://stackoverflow.com/a/12731439/358804
                var $descendants = $el.find(selector);
                return $el.filter(selector).add($descendants).filter(':not([data-toc-skip])');
            },

            generateUniqueIdBase: function (el) {
                var text = $(el).text();
                var anchor = text.trim().toLowerCase().replace(/[^A-Za-z0-9]+/g, '-');
                return anchor || el.tagName.toLowerCase();
            },

            generateUniqueId: function (el) {
                var anchorBase = this.generateUniqueIdBase(el);
                for (var i = 0; ; i++) {
                    var anchor = anchorBase;
                    if (i > 0) {
                        // add suffix
                        anchor += '-' + i;
                    }
                    // check if ID already exists
                    if (!document.getElementById(anchor)) {
                        return anchor;
                    }
                }
            },

            generateAnchor: function (el) {
                if (el.attr('id')) {
                    return el.attr('id');
                } else {
                    var anchor = this.generateUniqueId(el);
                    el.attr('id', anchor);
                    return anchor;
                }
            },

            createNavList: function () {
                return $('<ul class="nav ' + this.listClass + '"></ul>');
            },

            createChildNavList: function ($parent) {
                var $childList = this.createNavList();
                $parent.append($childList);
                return $childList;
            },

            generateNavEl: function (anchor, text) {
                var $a = $('<a class="nav-link ' + this.linkClass + '"></a>');
                $a.attr('href', '#' + anchor);
                $a.text(text);
                var $li = $('<li></li>');
                $li.append($a);
                return $li;
            },

            generateNavItem: function (headingEl) {
                var anchor = this.generateAnchor(headingEl);
                var $heading = $(headingEl);
                var text = $heading.data(this.titleAttribute) || $heading.text();
                return this.generateNavEl(anchor, text);
            },

            // returns the elements for the start level, and the next below it
            getHeadings: function ($scope, onlyAllowed) {
                var selector = '';
                selector += ':header';

                if (onlyAllowed) {
                    selector += '[data-toc-show]';
                }

                return $(selector, $scope);
            },

            getNavLevel: function (el) {
                return parseInt(el.prop('tagName').charAt(1), 10);
            },

            populateNav: function ($nav, $headings, startLevel, maxLevel) {
                var $prevNav;
                var $currentNavs = [];
                var $currentNav = $nav;
                var prevNavLevel = startLevel;
                var helpers = this;

                $currentNavs[startLevel] = $nav;

                $headings.each(function () {
                    var $el = $(this);
                    var navLevel = helpers.getNavLevel($el);
                    if (navLevel < startLevel || navLevel > maxLevel) {
                        return;
                    }

                    var $navItem = helpers.generateNavItem($el);

                    if (prevNavLevel < navLevel) {
                        $currentNav = helpers.createChildNavList($prevNav);
                        $currentNavs[navLevel] = $currentNav;
                    }
                    if (prevNavLevel > navLevel) {
                        if (typeof $currentNavs[navLevel] === 'undefined') {
                            $currentNav = $currentNavs[startLevel];
                        }
                        else {
                            $currentNav = $currentNavs[navLevel];
                        }
                    }

                    $currentNav.append($navItem);
                    $prevNav = $navItem;
                    prevNavLevel = navLevel;
                });
            },

            parseOps: function (arg) {
                var opts;
                if (arg.jquery) {
                    opts = {
                        $nav: arg
                    };
                } else {
                    opts = arg;
                }
                opts.$scope = opts.$scope || $(document.body);
                return opts;
            }
        },

        // accepts a jQuery object, or an options object
        init: function (opts) {
            this.helpers.listClass = opts.listClass;
            this.helpers.linkClass = opts.linkClass;

            opts = this.helpers.parseOps(opts);

            // ensure that the data attribute is in place for styling
            opts.$nav.attr('data-toggle', 'toc');

            var $topContext = this.helpers.createChildNavList(opts.$nav);
            var startLevel = opts.startLevel;
            var maxLevel = opts.maxLevel;
            var $headings = this.helpers.getHeadings(opts.$scope, opts.onlyAllowed);
            this.helpers.populateNav($topContext, $headings, startLevel, maxLevel);
        }
    };
})(jQuery);
