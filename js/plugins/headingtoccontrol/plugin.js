/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings, CKEDITOR) {

  function validateHeadingElement(element) {
    return element && !element.isReadOnly() && element.is('h1', 'h2', 'h3', 'h4', 'h5', 'h6')
  }

  function getSelectedHeading(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();
    if (selectedElement && selectedElement.is('h1', 'h2', 'h3', 'h4', 'h5', 'h6')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'], 1);
    }
    return null;
  }

  CKEDITOR.plugins.add('headingtoccontrol', {
    icons: 'HeadingTocControl',

    init: function init(editor) {
      CKEDITOR.dialog.add('headingTocControlDialog', this.path + 'dialogs/headingTocControlDialog.js');
      var dialogCommand = new CKEDITOR.dialogCommand( 'headingTocControlCommand' );
      dialogCommand.exec = function exec (editor) {
        if (!validateHeadingElement(getSelectedHeading(editor))) {
          return;
        }

        // var b = this.tabId;
        // var a = editor;
        editor.openDialog('headingTocControlDialog', function (editor) {this.tabId && editor.selectPage(this.tabId)});
      }

      editor.addCommand('headingTocControlCommand', dialogCommand);
      editor.setKeystroke(CKEDITOR.CTRL + 72, 'headingTocControlCommand');

      if (editor.ui.addButton) {
        editor.ui.addButton('HeadingTocControl', {
          label: Drupal.t('Table of Content settings'),
          command: 'headingTocControlCommand'
        });
      }

      editor.on('doubleclick', function (evt) {
        var element = getSelectedHeading(editor) || evt.data.element;

        if (validateHeadingElement(element)) {
          editor.getSelection().selectElement(element);
          editor.getCommand('headingTocControlCommand').exec();
        }
      });

      if (editor.contextMenu) {
        editor.addMenuGroup( 'heading' );
        editor.addMenuItem( 'headingTocItem', {
          label: 'Table of Content settings',
          icon: 'HeadingTocControl',
          command: 'headingTocControlCommand',
          group: 'heading'
        });

        editor.contextMenu.addListener( function( element ) {
          if (validateHeadingElement(element)) {
            return { headingTocItem: CKEDITOR.TRISTATE_OFF };
          }
        });
      }
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);