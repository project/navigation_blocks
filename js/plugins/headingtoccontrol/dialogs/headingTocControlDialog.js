(function (CKEDITOR, Drupal, window) {
  'use strict';

  // Register our settings dialog.
  CKEDITOR.dialog.add('headingTocControlDialog', function (editor) {
    return {
      icons: 'HeadingTocControl',
      title: Drupal.t('Table of Content settings'),
      minWidth: 310,
      minHeight: 200,

      contents: [
        {
          id: 'headingTocControlTab',
          label: Drupal.t('Manage Table of Content settings for headings'),

          elements: [
            {
              id: 'auto',
              type: 'radio',
              default: 'auto',
              items: [ [ Drupal.t('Automatic'), 'auto' ], [ Drupal.t('Show'), 'show' ], [ Drupal.t('Skip'), 'skip' ] ],
              label: Drupal.t('Control setting.')
            },
            {
              type: 'html',
              html: '<p>' + Drupal.t('Show: This controls if the Heading is always added to a "Table of Contents" block. <br/>' +
                'Skip: This controls if the Heading is always skipped for a "Table of Contents" block.</br>' +
                'Auto: This let\'s the "Table of Contents" block control if it\'s added.') + '</p>'
            },
            {
              id: 'data-toc-text',
              type: 'text',
              label: Drupal.t('Title')
            },
            {
              type: 'html',
              html: '<p>' + Drupal.t('This is the title to show in the "Table of Contents" link.') + '</p>'
            },

          ]
        }
      ],

      onShow: function () {
        var selection = editor.getSelection();
        var headingElement = getHeadingElement(selection);

        if (!headingElement) {
          return;
        }

        this.setValueOf('headingTocControlTab', 'data-toc-text', headingElement.getAttribute('data-toc-text'));

        var skip = headingElement.getAttribute('data-toc-skip');
        var show = headingElement.getAttribute('data-toc-allow');

        if (headingElement.getAttribute('data-toc-skip')) {
          this.setValueOf('headingTocControlTab', 'auto', 'skip');
          return;
        }
        else if (headingElement.getAttribute('data-toc-show')) {
          this.setValueOf('headingTocControlTab', 'auto', 'show');
          return;
        }

        this.setValueOf('headingTocControlTab', 'auto', 'auto');
      },

      onOk: function () {
        var editor = this.getParentEditor();
        var dataTocText = this.getValueOf('headingTocControlTab', 'data-toc-text');
        var auto = this.getValueOf('headingTocControlTab', 'auto');
        var selection = editor.getSelection();
        var headingElement = getHeadingElement(selection);

        if (!headingElement) {
          return;
        }

        headingElement.setAttribute('data-toc-text', dataTocText);
        headingElement.removeAttribute('data-toc-show');
        headingElement.removeAttribute('data-toc-skip');

        if (auto === 'skip') {
          headingElement.setAttribute('data-toc-skip', 'skip');
        }
        else if (auto === 'show') {
          headingElement.setAttribute('data-toc-show', 'show');

        }

        this.commitContent(headingElement);
      }
    };

    function getHeadingElement (selection) {
      var selectedElement = selection.getStartElement();
      if (selectedElement && selectedElement.is('h1', 'h2', 'h3', 'h4', 'h5', 'h6')) {
        return selectedElement;
      }

      var range = selection.getRanges(true)[0];

      if (range) {
        range.shrink(CKEDITOR.SHRINK_TEXT);
        return editor.elementPath(range.getCommonAncestor()).contains(['h1', 'h2', 'h3', 'h4', 'h5', 'h6'], 1);
      }

      return false;
    };
  });

})(CKEDITOR, Drupal, window);
