/**
 * @file
 * Initialize history.back logic.
 */

(function ($, Drupal, window, once) {
  'use strict';

  Drupal.behaviors.navigationBlocksBackButton = {
    attach: function (context) {
      once('navigationBlockHistoryBack', context.querySelectorAll('a.js-history-back')).forEach(function (element) {
        $(element).on('click', function (event) {
          window.history.back(-1);
          event.preventDefault();
        });
      });
    }
  };

}(jQuery, Drupal, window, once));
