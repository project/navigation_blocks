/**
 * @file
 * Crawls the page for menu titles to display in the table of contents block.
 */

(function ($, Drupal, drupalSettings, once) {
  'use strict';

  Drupal.behaviors.tocCrawlMenuTitles = {
    attach: function () {
      once('tocCrawlMenuTitles', document.querySelectorAll('.js-toc')).forEach(function (element) {
        var maxHeadingLevel = parseInt(drupalSettings.navigation_blocks.toc.max_heading_level);
        var wrapper = drupalSettings.navigation_blocks.toc.wrapper;
        var onlyAllowed = drupalSettings.navigation_blocks.toc.only_allowed;
        var listClass = drupalSettings.navigation_blocks.toc.list_class;
        var linkClass = drupalSettings.navigation_blocks.toc.link_class;
        var $scope;
        if (wrapper) {
          $scope = $(wrapper);
        }

        Toc.init({
          $nav: $(element),
          $scope: $scope,
          maxLevel: maxHeadingLevel,
          startLevel: 2,
          onlyAllowed: onlyAllowed,
          listClass: listClass,
          linkClass: linkClass
        });
      });
    }
  };

}(jQuery, Drupal, drupalSettings, once));
