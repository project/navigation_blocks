<?php

namespace Drupal\navigation_blocks;

/**
 * Builds a table of contents block.
 *
 * @package Drupal\navigation_blocks
 */
class TocBuilder {

  /**
   * The maximum heading level the block will display.
   *
   * @var string
   */
  protected $maxHeadingLevel;

  /**
   * The wrapper in which the block will look for headings.
   *
   * @var string
   */
  protected $wrapper = '';

  /**
   * If only allowed headings should be added, or all.
   *
   * @var bool
   */
  protected $onlyAllowed = false;

  /**
   * Class to use for styling the table of contents list.
   *
   * @var string
   */
  protected $listClass = '';

  /**
   * Class to use for styling a table of contents link.
   *
   * @var string
   */
  protected $linkClass = '';

  /**
   * Gets the maximum heading level the block will display.
   *
   * @return string
   *   The maximum heading level the block will display.
   */
  public function getMaxHeadingLevel(): string {
    return $this->maxHeadingLevel;
  }

  /**
   * Gets the wrapper in which the block will look for headings.
   *
   * @return string
   *   The wrapper in which the block will look for headings.
   */
  public function getWrapper(): string {
    return $this->wrapper;
  }

  /**
   * Gets the class a heading should have to be displayed in the block.
   *
   * @return string
   *   The class a heading should have to be displayed in the block.
   */
  public function getOnlyAllowed(): string {
    return $this->onlyAllowed;
  }

  /**
   * Gets the class to use for styling the table of contents list.
   *
   * @return string
   *   The class to use for styling the table of contents list.
   */
  public function getListClass(): string {
    return $this->listClass;
  }

  /**
   * Gets the class to use for styling a table of contents link.
   *
   * @return string
   *   The class to use for styling a table of contents link.
   */
  public function getLinkClass(): string {
    return $this->linkClass;
  }

  /**
   * Sets the maximum heading level the block will display.
   *
   * @param string $maxHeadingLevel
   *   The maximum heading level the block will display.
   *
   * @return \Drupal\navigation_blocks\TocBuilder
   *   The TocBuilder.
   */
  public function setMaxHeadingLevel(string $maxHeadingLevel): TocBuilder {
    $this->maxHeadingLevel = $maxHeadingLevel;
    return $this;
  }

  /**
   * Sets the wrapper in which the block will look for headings.
   *
   * @param string $wrapper
   *   The wrapper in which the block will look for headings.
   *
   * @return \Drupal\navigation_blocks\TocBuilder
   *   The TocBuilder.
   */
  public function setWrapper(string $wrapper): TocBuilder {
    $this->wrapper = $wrapper;
    return $this;
  }

  /**
   * Sets the class a heading should have to be displayed in the block.
   *
   * @param string $requiredClass
   *   The class a heading should have to be displayed in the block.
   *
   * @return \Drupal\navigation_blocks\TocBuilder
   *   The TocBuilder.
   */
  public function setOnlyAllowed(bool $onlyAllowed): TocBuilder {
    $this->onlyAllowed = $onlyAllowed;
    return $this;
  }

  /**
   * Sets the class to use for styling the table of contents list.
   *
   * @param string $listClass
   *   The class to use for styling the table of contents list.
   *
   * @return \Drupal\navigation_blocks\TocBuilder
   *   The TocBuilder.
   */
  public function setListClass(string $listClass): TocBuilder {
    $this->listClass = $listClass;
    return $this;
  }

  /**
   * Sets the class to use for styling a table of contents link.
   *
   * @param string $linkClass
   *   The class to use for styling a table of contents link.
   *
   * @return \Drupal\navigation_blocks\TocBuilder
   *   The TocBuilder.
   */
  public function setLinkClass(string $linkClass): TocBuilder {
    $this->linkClass = $linkClass;
    return $this;
  }

  /**
   * Builds and returns the renderable array for the TocBlock plugin.
   *
   * @return array
   *   A renderable array representing the content of the block.
   */
  public function build(): array {
    return [
      'nav' => [
        '#type' => 'html_tag',
        '#tag' => 'nav',
        '#attributes' => [
          'class' => [
            'toc-navigation',
            'scrollspy-menu',
            'js-toc',
          ],
        ],
        '#attached' => [
          'library' => ['navigation_blocks/toc'],
          'drupalSettings' => [
            'navigation_blocks' => [
              'toc' => [
                'max_heading_level' => $this->getMaxHeadingLevel(),
                'wrapper' => $this->getWrapper(),
                'only_allowed' => $this->getOnlyAllowed(),
                'list_class' => $this->getListClass(),
                'link_class' => $this->getLinkClass(),
              ],
            ],
          ],
        ],
      ],
    ];
  }

}
