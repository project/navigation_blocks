<?php

namespace Drupal\navigation_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\navigation_blocks\TocBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Table of contents' block.
 *
 * @Block(
 *   id = "toc",
 *   admin_label = @Translation("Table of contents"),
 * )
 */
class TocBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Builds a table of contents block.
   *
   * @var \Drupal\navigation_blocks\TocBuilder
   */
  protected $tocBuilder;

  /**
   * TocBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\navigation_blocks\TocBuilder $toc_builder
   *   The TocBuilder.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TocBuilder $toc_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->tocBuilder = $toc_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('navigation_blocks.toc_builder'));
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'max_heading_level' => '',
      'wrapper' => '',
      'required_class' => '',
      'title_attribute' => '',
      'offset' => '',
      'list_class' => '',
      'link_class' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['max_heading_level'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum heading level'),
      '#description' => $this->t('The maximum heading level the block will display. The minimum heading level is 2.'),
      '#default_value' => $this->configuration['max_heading_level'],
    ];

    $form['wrapper'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Wrapper'),
      '#description' => $this->t('The wrapper in which the block will look for headings. This should be a jQuery selector'),
      '#default_value' => $this->configuration['wrapper'],
    ];

    $form['only_allowed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only allowed'),
      '#description' => $this->t('Only allow headings with data-toc-show attribute.'),
      '#default_value' => $this->configuration['only_allowed'],
    ];

    $form['list_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List class'),
      '#description' => $this->t('Class to use for styling the table of contents list.'),
      '#default_value' => $this->configuration['list_class'],
    ];

    $form['link_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link class'),
      '#description' => $this->t('Class to use for styling a table of contents link.'),
      '#default_value' => $this->configuration['link_class'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $this->setConfigurationValue('max_heading_level', $form_state->getValue('max_heading_level'));
    $this->setConfigurationValue('wrapper', $form_state->getValue('wrapper'));
    $this->setConfigurationValue('only_allowed', (bool) $form_state->getValue('only_allowed'));
    $this->setConfigurationValue('title_attribute', $form_state->getValue('title_attribute'));
    $this->setConfigurationValue('list_class', $form_state->getValue('list_class'));
    $this->setConfigurationValue('link_class', $form_state->getValue('link_class'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $this->tocBuilder
      ->setMaxHeadingLevel($this->configuration['max_heading_level'])
      ->setWrapper($this->configuration['wrapper'])
      ->setOnlyAllowed($this->configuration['only_allowed'])
      ->setListClass($this->configuration['list_class'])
      ->setLinkClass($this->configuration['link_class']);

    return $this->tocBuilder->build();
  }

}
