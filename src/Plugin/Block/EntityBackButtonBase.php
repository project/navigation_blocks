<?php

namespace Drupal\navigation_blocks\Plugin\Block;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;

/**
 * Base class for entity buttons.
 *
 * @package Drupal\navigation_blocks\Plugin\Block
 */
abstract class EntityBackButtonBase extends BackButton {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function build(): array {
    try {
      $entity = $this->getEntity();
    }
    catch (EntityMalformedException $e) {
      return [];
    }

      if ($entity && !$entity->isNew()) {
      $link = $entity->toLink()->toRenderable();
      $this->backButtonManager->addLinkAttributes($link);
      return $link;
    }

    $build = parent::build();
    if (!empty($build)) {
      return $build;
    }

    return [];
  }

  /**
   * Get the for this block.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  abstract protected function getEntity(): EntityInterface;

}
