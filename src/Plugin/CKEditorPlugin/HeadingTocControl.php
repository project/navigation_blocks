<?php

namespace Drupal\navigation_blocks\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "headingtoccontrol" plugin.
 *
 * @CKEditorPlugin(
 *   id = "headingtoccontrol",
 *   label = @Translation("Heading Table of Contents Attributes"),
 *   module = "navigation_blocks"
 * )
 */
class HeadingTocControl extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return \Drupal::service('extension.list.module')->getPath('navigation_blocks') . '/js/plugins/headingtoccontrol/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $path = \Drupal::service('extension.list.module')->getPath('navigation_blocks') . '/js/plugins/headingtoccontrol';
    return [
      'HeadingTocControl' => [
        'label' => $this->t('Table of Contents Control'),
        'image' => $path . '/icons/headingtoccontrol.png',
      ],
    ];
  }

}
